/** \defgroup piano
 * \brief Driver for a giant piano MIDI instrument.
 *
 * Currently only implemented on the TIVA C.
 *
 * \author Josh Klodnicki
 * \author Jacob Cooper
 * \author Kevin Poretti
 *
 * \pre    \ref hal_piano
 * \pre    Embeddedlibrary modules:
 *          - MIDI
 *          - UART (if using UART)
 *          - MIDI_BLE (if using Bluetooth)
 *
 * \todo   Document macro settings
 *
 * @{
 */

#ifndef PIANO_H
#define PIANO_H

/** Initializes the piano module.
 *
 * \author Josh Klodnicki
 * \author Jacob Cooper
 */
void piano_init(void);

/** Processes which piano keys are pressed.
 *
 * Called by the timer interrupt service routine in hal_piano. Not intended
 * to be called manually by the user, though it could be used to override
 * the automatic timing.
 *
 * \author Josh Klodnicki
 * \author Jacob Cooper
 * \pre    Module must have been initialized.
 *
 * \todo Can this prototype be moved to the top of hal_piano.c?
 */
void piano_process_keys(void);

#endif

///@}
