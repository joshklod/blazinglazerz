/** \ingroup piano
	@{
 */

#include "system.h"
#include "hal_piano.h"
#include <stdint.h>

#ifndef PIANO_H
	#error "Please include piano.h in system.h"
#endif

#if defined PIANO_MODE_UART && defined PIANO_MODE_BLE
#warning "Both UART and BLE specified for piano mode. Defaulting to UART."
#undef PIANO_MODE_BLE
#elif !(defined PIANO_MODE_UART || defined PIANO_MODE_BLE)
/** Specifies the MIDI communication channel of the piano module
 * 
 * Define either `PIANO_MODE_UART` or `PIANO_MODE_BLE` in `system.h` to select
 * the mode. Default is `PIANO_MODE_UART`.
 */
#define PIANO_MODE_UART
#endif

#ifndef PIANO_MIDI_CHANNEL
/** The MIDI channel on which the piano operates.
 * Can be overridden in `system.h`.
 */
#define PIANO_MIDI_CHANNEL 3
#endif
#ifndef PIANO_UART_CHANNEL
/** The UART channel on which the piano MIDI operates.
 * Can be overridden in `system.h`.
 */
#define PIANO_UART_CHANNEL 0
#endif

#define MIDI_CHANNEL PIANO_MIDI_CHANNEL

/** Array mapping piano keys to input pins */
char pianoMap[4][14] = {
		/*  initializers for row indexed by 0 white keys*/
		{'e','d','c','b','a','g','f','E','G','F','A','D','C','B'},
		/*  initializers for row indexed by 1 black keys*/
		/* h=f#, i=g#, j=a#, k=c#, l=d#, m=F#, n=G#, o=A#, p=C#, q=D# */
		{0,  'l','k',  0,'j','i','m',  0,'m','p','n','p',  0,'o'},
		/*  initializers for row indexed by 2 instrument selection*/
		/* 8=piano,9=sax,Q=violin,W=clarinet,R=trump,T=banjo,Y=xylo,U=guitar*/
		{0, 'W', 0, 'Q', '9', '8', 0, 'U', 0, 'R', 'T', 0, 'Y', 0},
		/*  initializers for row indexed by 3 menu*/
		/* 1=play, 2=demo, 3=playback, 4=record, 5=volume down, 6=volume up */
		{0, 0, 0, '4', 0, '5', 0, 0, '1', '2', 0, 0, 0, 0}
};

/** Enumeration to track which pulse we're on */
typedef enum {
	FIRST_BIT   = 0x01, ///< Same value as rightmost bit (for wrapping)
	WHITE       = 0x01,
	BLACK       = 0x02,
	INSTRUMENTS = 0x04,
	CONTROL     = 0x08,
	LAST_BIT    = 0x08  ///< Same value as leftmost bit (for wrapping)
} piano_t;

/* Function Definitions */

/** Sends a MIDI command over UART or BLE
 *
 * \author Jacob Cooper
 * \brief tells what to transmit the MIDI data over.
 *
 * Can send Midi data over UART or the bluefruit BLE depending
 * upon specifying PIANO_MODE_UART or PIANO_MODE BLE
 *
 *\param ptr - pointer to the data array that is to be sent
 *\param len - the number of data elements that will be sent
 */
void transmitMIDI(uint8_t * ptr, uint8_t len) {
#ifdef PIANO_MODE_UART
	UART_Write(PIANO_UART_CHANNEL, ptr, len);
#elif defined PIANO_MODE_BLE
	MIDI_BLE_Send(ptr,len);
#endif
}

void piano_init(void) {
    hal_piano_init();
#ifdef PIANO_MODE_BLE
	MIDI_BLE_Init();
#endif
	MIDI_Init(transmitMIDI);
}

/**
 * \author Jacob Cooper
 * \brief Maps the note that was pressed to the MIDI note.
 *
 * The midi note and whether or not the note was pressed or released is given
 * and then a large case statement deals with each note or command depending
 * upon how it is mapped
 *
 * \param note - the character that is used to represent the note
 * \param pressed - whether the command/note was released or pressed
 */
void sendMidi(char note, uint8_t pressed) //- calls midiBLE
{
	switch(note){
	case 'G':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 67, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 67);
		break;
	case 'F':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 65, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 65);
		break;
	case 'E':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 76, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 76);
		break;
	case 'D':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 74, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 74);
		break;
	case 'C':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 72, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 72);
		break;
	case 'B':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 71, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 71);
		break;
	case 'f':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 53, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 53);
		break;
	case 'A':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 69, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 69);
		break;
	case 'c':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 60, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 60);
		break;
	case 'g':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 55, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 55);
		break;
	case 'd':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 62, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 62);
		break;
	case 'b':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 59, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 59);
		break;
	case 'a':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 57, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 57);
		break;
	case 'e':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 64, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 64);
		break;
	case 'l':/* h=f#, i=g#, j=a#, k=c#, m=F#, n=G#, o=A#, p=C#, q=D# */
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 63, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 63);
		break;
	case 'k':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 61, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 61);
		break;
	case 'j':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 58, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 58);
		break;
	case 'i':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 56, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 56);
		break;
	case 'h':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 54, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 54);
		break;
	case 'q':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 75, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 75);
		break;
	case 'p':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 73, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 73);
		break;
	case 'o':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 70, 127);//up octave A#
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 70);
		break;
	case 'm':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 66, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 66);
		break;
	case 'n':
		if(pressed)
			MIDI_SendNote(MIDI_CHANNEL, 68, 127);
		else
			MIDI_SendNoteOff(MIDI_CHANNEL, 68);
		break;
		/* 8=piano,9=sax,Q=violin,W=clarinet,R=trump,T=banjo,Y=xylo,U=guitar*/
	case '8':
		MIDI_ProgramChange(MIDI_CHANNEL, 1);
		break;
	case '9':
		MIDI_ProgramChange(MIDI_CHANNEL, 65);
		break;
	case 'Q':
		MIDI_ProgramChange(MIDI_CHANNEL, 41);
		break;
	case 'W':
		MIDI_ProgramChange(MIDI_CHANNEL, 81);
		break;
	case 'R':
		MIDI_ProgramChange(MIDI_CHANNEL, 57);
		break;
	case 'T':
		MIDI_ProgramChange(MIDI_CHANNEL, 36);
		break;
	case 'Y':
		MIDI_ProgramChange(MIDI_CHANNEL, 9);
		break;
	case 'U':
		MIDI_ProgramChange(MIDI_CHANNEL, 25);
		break;
	default:
		break;
	}
}

/**
 * \author Jacob Cooper
 * \brief this function goes through each note and seperates it by section.
 *
 * This function uses a mask and bit shifts through the keys. Since the
 * keys are a combination of the different GPIOs put together, each bit is
 * checked against the mask. If it is a 1, which means it was pressed or depressed,
 * then the note is sent based on the section that was sampled.
 *
 * \param section - Specifies what part of the piano was sampled ex, WHITE, BLACK, INSTRUMENTS, COMMANDS and more
 * \param keys - the bit packed GPIO pins of from the Uc that represents a button
 * \param pressed - whether the data sent was for the released or pressed notes
 *
 */
void ProcessMidi(piano_t section, uint16_t keys, uint8_t pressed){
uint16_t mask = 0x0001;
uint16_t key = keys;
uint8_t i;
uint16_t test;

for(i = 0; i< 14; i++){
	test = mask&key;
	if( test == 0x0001){
			switch(section){
			case WHITE:
				sendMidi(pianoMap[0][i],pressed);
				break;
			case BLACK:
				sendMidi(pianoMap[1][i],pressed);
				break;
			case INSTRUMENTS:
				sendMidi(pianoMap[2][i],pressed);
				break;
			case CONTROL:
				sendMidi(pianoMap[3][i],pressed);
				break;
			}
		}
	key >>= 1;
	}
}


void piano_process_keys(void) {

	static piano_t pulse = FIRST_BIT;

	// Static struct storing the state of each set of buttons
	static struct {
		uint16_t white;
		uint16_t black;
		uint16_t inst;
		uint16_t ctrl;
	} buttons = {0, 0, 0, 0};

	// Get which buttons are pressed from GPIO pins
    uint16_t input = hal_piano_get_input();

    // Pointer to current set of buttons
    uint16_t *keyset;
    // Select appropriate set depending on current pulse
    switch (pulse) {
		case WHITE:       keyset = &(buttons.white); break;
		case BLACK:       keyset = &(buttons.black); break;
		case INSTRUMENTS: keyset = &(buttons.inst);  break;
		case CONTROL:     keyset = &(buttons.ctrl);  break;
    }

    // Handle button changes
    if (input != *keyset) {
    	// A key has been pressed or released

    	// Get key changes
    	uint16_t pressed_keys  = ~(*keyset) &  input;
    	uint16_t released_keys =   *keyset  & ~input;

        switch (pulse) {
    		case WHITE:
    			ProcessMidi(WHITE, pressed_keys, 1);
    			ProcessMidi(WHITE, released_keys, 0);
    			break;
    		case BLACK:
    			ProcessMidi(BLACK, pressed_keys, 1);
    			ProcessMidi(BLACK, released_keys, 0);
    			break;
    		case INSTRUMENTS:
    			ProcessMidi(INSTRUMENTS, pressed_keys, 1);
    			ProcessMidi(INSTRUMENTS, released_keys, 0);
    			break;
    		case CONTROL:
    			ProcessMidi(CONTROL, pressed_keys, 1);
    			ProcessMidi(CONTROL, released_keys, 0);
    			break;
        }

        // Update button states
        *keyset = input;
    }
    // Otherwise, no action necessary

    // Shift the pulse
    pulse <<= 1;
    // Wrap at bit 4
    if (pulse > LAST_BIT) pulse = FIRST_BIT;
    // Update the output
    hal_piano_set_output(pulse);
}

///@}
