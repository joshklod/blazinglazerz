README
======

Blazing Lazerz Embedded Library
-------------------------------

This is a set of embedded modules developed by the Blazing Lazerz team at 
Rowan University.

### Modules ###

- \ref piano

### Team Members ###

- Jacob Cooper
- Josh Klodnicki <<klodni21@students.rowan.edu>>
- Kevin Poretti


\see \ref report
\see \ref slack_trello

