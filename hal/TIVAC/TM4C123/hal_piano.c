/** \ingroup hal_piano
	@{
 */

#include "system.h"
#include "hal_piano.h"
#include "piano.h"
#include <stdint.h>
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/gpio.h"
#include "inc/hw_memmap.h"
#include "macros.h"

/* Error checking */
#ifndef HAL_PIANO_H
#error "Please include hal_piano.h in system.h"
#endif
#ifndef PERIPHERAL_CLOCK
#error "Please define PERIPHERAL_CLOCK in system.h"
#endif

/* Default Settings */
#ifndef PIANO_TIMER
/** Timer peripheral used by the piano.
 * Can be overridden in `system.h` (default 0).
 */
	#define PIANO_TIMER        0
#elif PIANO_TIMER>5
	#error "PIANO_TIMER must be 5 or less on this processor"
#endif
#ifndef PIANO_SUBTIMER
/** 16-bit sub-timer (A or B) used by the piano.
 * Can be overridden in `system.h` (default A).
 */
	#define PIANO_SUBTIMER     A
#endif

#ifndef PIANO_OUT_PORT
/** GPIO port on which to send the 4-bit wide pulse.
 * Can be overridden in `system.h` (default D).
 */
	#define PIANO_OUT_PORT     D
#endif
#ifndef PIANO_IN_PORT_LOW
/** GPIO port used for bits [7:0] of the input.
 * Can be overridden in `system.h` (default B).
 */
	#define PIANO_IN_PORT_LOW  B
#endif
#ifndef PIANO_IN_PORT_HIGH
/** GPIO port used for bits [13:8] of the input.
 * Can be overridden in `system.h` (default E).
 */
	#define PIANO_IN_PORT_HIGH E
#endif

#ifndef PIANO_TIMER_PERIOD_US
/** Period between each sample of the piano keyboard in microseconds.
 * Each sample is only one of the four rows, so the total time between reads
 * of each key is `4*PIANO_TIMER_PERIOD_US`. Can be overridden in `system.h`
 * (default 250).
 */
	#define PIANO_TIMER_PERIOD_US 250 // In microseconds
#endif

/* Derived Macros */
#define PIANO_TIMER_PERIOD_LOAD\
		(PIANO_TIMER_PERIOD_US*PERIPHERAL_CLOCK/1000000)

#define PIANO_TIMER_CTL         CAT2(SYSCTL_PERIPH_TIMER, PIANO_TIMER)
#define PIANO_TIMER_BASE        CAT3(TIMER, PIANO_TIMER, _BASE)
#define PIANO_SUBTIMER_MODE     CAT2(TIMER_, PIANO_SUBTIMER)
#define PIANO_TIMER_IFG         CAT3(TIMER_TIM, PIANO_SUBTIMER, _TIMEOUT)

#define PIANO_GPIO_OUT_CTL      CAT2(SYSCTL_PERIPH_GPIO, PIANO_OUT_PORT)
#define PIANO_GPIO_OUT_BASE     CAT3(GPIO_PORT, PIANO_OUT_PORT, _BASE)
#define PIANO_GPIO_INL_CTL      CAT2(SYSCTL_PERIPH_GPIO, PIANO_IN_PORT_LOW)
#define PIANO_GPIO_INL_BASE     CAT3(GPIO_PORT, PIANO_IN_PORT_LOW, _BASE)
#define PIANO_GPIO_INH_CTL      CAT2(SYSCTL_PERIPH_GPIO, PIANO_IN_PORT_HIGH)
#define PIANO_GPIO_INH_BASE     CAT3(GPIO_PORT, PIANO_IN_PORT_HIGH, _BASE)

/* Function Prototypes */

/** Interrupt Service Routine for Piano module
 * 
 * Calls \ref piano_process_keys from piano.h.
 *
 * \author Josh Klodnicki
 */
void hal_piano_isr(void);

/* Function Definitions */

void hal_piano_init(void) {
	// Enable Peripherals
	SysCtlPeripheralEnable(PIANO_TIMER_CTL);
	SysCtlPeripheralEnable(PIANO_GPIO_OUT_CTL);
	SysCtlPeripheralEnable(PIANO_GPIO_INL_CTL);
	SysCtlPeripheralEnable(PIANO_GPIO_INH_CTL);

	// Configure Timer
	TimerLoadSet(PIANO_TIMER_BASE, PIANO_SUBTIMER_MODE,
			PIANO_TIMER_PERIOD_LOAD);
	TimerIntClear(PIANO_TIMER_BASE, PIANO_TIMER_IFG);
	TimerConfigure(PIANO_TIMER_BASE, TIMER_CFG_PERIODIC);
	TimerIntRegister(PIANO_TIMER_BASE, PIANO_SUBTIMER_MODE, hal_piano_isr);
	TimerEnable(PIANO_TIMER_BASE, PIANO_SUBTIMER_MODE);
	TimerIntEnable(PIANO_TIMER_BASE, PIANO_TIMER_IFG);

    // Configure GPIO Pins
	GPIOPinTypeGPIOOutput(PIANO_GPIO_OUT_BASE, 0x0F);
	/// \todo what does all this do?
#if (PIANO_IN_PORT_LOW == B || PIANO_IN_PORT_HIGH == B)
     HWREG(GPIO_PORTB_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
	 HWREG(GPIO_PORTB_BASE + GPIO_O_CR) |= 0x0C;
	 HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) &= ~0x0C;
	 HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= 0x0C;
	 HWREG(GPIO_PORTB_BASE + GPIO_O_LOCK) = 0;
#endif
	GPIOPinTypeGPIOInput(PIANO_GPIO_INL_BASE, 0xFF);
	GPIOPinTypeGPIOInput(PIANO_GPIO_INH_BASE, 0x3F);

}

uint16_t hal_piano_get_input(void) {
	union16_t input;
	input.b[0] = GPIOPinRead(PIANO_GPIO_INL_BASE, 0xFF);
	input.b[1] = GPIOPinRead(PIANO_GPIO_INH_BASE, 0x3F);
    return input.word;
}

void hal_piano_set_output(uint8_t data) {
	GPIOPinWrite(PIANO_GPIO_OUT_BASE, 0x0F, data);
}

void hal_piano_isr(void) {
	piano_process_keys();
	TimerIntClear(PIANO_TIMER_BASE, PIANO_TIMER_IFG);
}

///@}
