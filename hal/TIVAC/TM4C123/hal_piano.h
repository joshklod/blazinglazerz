/** \defgroup hal_piano
 *
 *	\ingroup piano
 *	\author Josh Klodnicki
 *	\pre \ref piano
 *	\pre TivaWare Peripheral Driver Library
 *	\pre Embeddedlibrary
 *        - macros.h
 *	\pre Define PERIPHERAL_CLOCK in system.h
 *
 *	@{
 */

#ifndef HAL_PIANO_H
#define HAL_PIANO_H

#include <stdint.h>

/** Initializes the piano module HAL.
 *
 * Configures the GPIO pins and timer module, and starts the timer.
 * 
 * \author Josh Klodnicki
 */
void hal_piano_init(void);

/** Gets the current state of the keys from the input pins
 *
 * \author Josh Klodnicki
 * \returns the state of the piano keys
 */
uint16_t hal_piano_get_input(void);

/** Sets the output pulse being sent to the piano keys
 *
 * \author Josh Klodnicki
 * \param data Output pins will be set to data[3:0]
 */
void hal_piano_set_output(uint8_t data);

#endif

///@}
